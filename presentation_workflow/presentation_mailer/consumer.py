import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)
            body_dict = json.loads(body)
            send_mail(
                "Your presentation has been accepted",
                f'Dear {body_dict["presenter_name"]},We are happy to tell you that your presentation, {body_dict["title"]}, has been accepted!',
                "admin@conference.go",
                [f'{body_dict["presenter_email"]}'],
                fail_silently=False,
            )

        def process_rejected(ch, method, properties, body):
            print("  Received %r" % body)
            body_dict = json.loads(body)
            send_mail(
                "Sorry to tell you this",
                f'Dear {body_dict["presenter_name"]},We are sorry to tell you that your presentation, {body_dict["title"]}, sucks ass! Hope you have a great day!',
                "admin@conference.go",
                [f'{body_dict["presenter_email"]}'],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approval")
        channel.basic_consume(
            queue="presentation_approval",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejection")
        channel.basic_consume(
            queue="presentation_rejection",
            on_message_callback=process_rejected,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
